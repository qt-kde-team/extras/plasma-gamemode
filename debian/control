Source: plasma-gamemode
Section: kde
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 6.0.0),
               libkf6coreaddons-dev (>= 6.0.0),
               libkf6dbusaddons-dev (>= 6.0.0),
               libkf6declarative-dev (>= 6.0.0),
               libkf6i18n-dev (>= 6.0.0),
               libplasma-dev (>= 6.0.0),
               pkg-kde-tools,
               qt6-base-dev,
Standards-Version: 4.6.0
Homepage: https://invent.kde.org/sitter/plasma-gamemode
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/plasma-gamemode
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/plasma-gamemode.git

Package: plasma-gamemode
Architecture: any
Depends: plasma-desktop,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Plasma extension that monitors the current status of gamemode
 GameMode optimises your system for peak performance when running a game.
 .
 This shell extension will show you whether GameMode is active and for which
 processes.
 .
 If you have the Debian gamemode package installed, you can test this
 extension by running: /usr/games/gamemode-simulate-game
 which will show a tray icon for gamemode listing that process, and
 will stop after 10 seconds.
